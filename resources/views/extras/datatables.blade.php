{{--
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.bootstrap4.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js" defer></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js" defer></script>
<script src="https://cdn.datatables.net/plug-ins/1.11.3/dataRender/ellipsis.js" defer></script> --}}
<link href="assets\plugins\datatables\css\dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="assets\plugins\datatables\css\buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<script src="assets\plugins\datatables\jquery.dataTables.min.js" defer></script>
<script src="assets\plugins\datatables\dataTables.bootstrap4.min.js" defer></script>
<script src="assets\plugins\datatables\dataTables.buttons.min.js" defer></script>
<script src="assets\plugins\datatables\buttons.bootstrap4.min.js" defer></script>
<script src="assets\plugins\datatables\jszip.min.js" defer></script>
<script src="assets\plugins\datatables\pdfmake.min.js" defer></script>
<script src="assets\plugins\datatables\vfs_fonts.js" defer></script>
<script src="assets\plugins\datatables\buttons.html5.min.js" defer></script>
<script src="assets\plugins\datatables\buttons.colVis.min.js" defer></script>
<script src="assets\plugins\datatables\buttons.print.min.js" defer></script>
<script src="assets\plugins\datatables\dataTables.responsive.min.js" defer></script>
<script src="assets\plugins\datatables\ellipsis.js" defer></script>
<script src="{{ asset('assets/js/datatables.init.js') }}" defer></script>

{{-- https://code.jquery.com/jquery-3.5.1.js --}}




{{-- <script>
    console.log("a")
</script> --}}